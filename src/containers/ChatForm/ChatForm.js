import React, {useState} from 'react';
import axios from "axios";
import './ChatForm.css';

const ChatForm = () => {

    const [inputVal, setInputVal] = useState({
        author: '',
        message: ''
    });

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = new URLSearchParams();
        data.set('author', inputVal.author);
        data.set('message', inputVal.message);

        await axios.post('http://146.185.154.90:8000/messages', data)
            .then(res => {
                console.log(res)
            })
        setInputVal(inputVal => ({...inputVal, author: '', message: ''}));
    }

    const handleChange = (event) => {

        const inputValCopy = {...inputVal};

        inputValCopy[event.target.name] = event.target.value;

        setInputVal(inputValCopy);
    }

    return (
        <form className="chat-form" onSubmit={handleSubmit}>
            <input type="text" name="author" placeholder="enter ur name:" value={inputVal.author} onChange={handleChange}/>
            <input type="text" name="message" placeholder="here ur message:" value={inputVal.message} onChange={handleChange}/>
            <button>Send</button>
        </form>
    );
};

export default ChatForm;