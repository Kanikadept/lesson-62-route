import React from 'react';
import './Home.css';

const Home = props => {

    const handleChangeToChat = () => {
        props.history.push({
            pathname: '/chat'
        });
    };
    const handleChangeToCountry = () => {
        props.history.push({
            pathname: '/country'
        });
    };
    const handleChangeToFIlm = () => {
        props.history.push({
            pathname: '/films'
        });
    };

    return (
        <div className="home">
            <p>Go to home work</p>
            <button onClick={handleChangeToChat}>Chat</button>
            <button onClick={handleChangeToCountry}>Country-Info</button>
            <button onClick={handleChangeToFIlm}>Film-tracker</button>
        </div>
    );
};

export default Home;