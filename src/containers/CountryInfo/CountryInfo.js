import React from 'react';
import './CountryInfo.css';

const CountryInfo = ({selectedCountry}) => {

    let countryDiv = null;
    if (selectedCountry === null) {
        countryDiv = <div>Choose country!</div>
    } else {
        countryDiv =
            <div>
                {
                    <>
                        <p>{selectedCountry.name}</p>
                        <p><strong>Capital: </strong>{selectedCountry.capital}</p>
                        <p><strong>Population: </strong>{selectedCountry.population}</p>
                        <div className="borders">
                            <p>Borders with:</p>
                            <ul>{selectedCountry.borders.map(border => {
                                return <li key={border}>{border}</li>
                            })}</ul>
                        </div>
                    </>
                }
            </div>
    }


    return (
        <div className="country-info">
            {countryDiv}
        </div>
    );
};

export default CountryInfo;