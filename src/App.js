import './App.css';
import Chat from "./containers/Chat/Chat";
import Country from "./containers/Country/Country";
import FilmTracker from "./containers/FilmTracker/FilmTracker";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";

const App = () => (

    <BrowserRouter>
        <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/chat"  component={Chat}/>
            <Route path="/country"  component={Country}/>
            <Route path="/films"  component={FilmTracker}/>
        </Switch>
    </BrowserRouter>
);

export default App;
