import React, {useState,useEffect} from 'react';
import axios from "axios";
import './Chat.css';
import Message from "../../components/Message/Message";
import ChatForm from "../ChatForm/ChatForm";

const Chat = () => {

    const [messages, setMessages]  = useState([]);
    const [lastMesTime, setLastMesTime] = useState('');

    useEffect(() => {
        axios.get('http://146.185.154.90:8000/messages')
            .then(res => {
                // console.log(res);
                setMessages(res.data);
                setLastMesTime(res.data.slice(-1).pop().datetime);
            })
            .catch(err => {
                console.log(err);
            })
    }, [])


    useEffect(() => {

        const fetchData = async () => {

            const request = await axios.get('http://146.185.154.90:8000/messages?datetime=' + lastMesTime);
            if (request.data.length > 0) {
                setMessages([...messages, ...request.data]);

                setLastMesTime(request.data.slice(-1).pop().datetime);
            }
        }

        const interval = setInterval(() => {
            fetchData();
        }, 3000);

        console.log(interval)
        return () => clearInterval(interval);
    }, [lastMesTime])

    return (
        <div className="chat">
            <ChatForm />
            {
                messages.slice(0).reverse().map(message => {
                    return <Message key={message._id}
                                    author={message.author}
                                    text={message.message}
                                    time={message.datetime}/>
                })
            }
            {lastMesTime}
        </div>
    );
};

export default Chat;