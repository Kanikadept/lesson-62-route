import React from 'react';
import './ListOfCountries.css';

const ListOfCountries = ({countries, selectCountry}) => {


    const handleClick = (name) => {
        selectCountry(name);
    }

    return (
        <div className="list-of-countries">
            {countries.map(country => {
                return <div className="country-from-list"
                            key={country.alpha3Code}
                            onClick={() => {handleClick(country.alpha3Code)}}>
                    {country.name}
                </div>
            })}
        </div>
    );
};

export default ListOfCountries;