import React, {Component} from 'react';
import './Film.css';

class Film extends Component {

    constructor(props) {
        super(props);
    }

    changeHandler(name) {
        this.setState({name: name});
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.name !== this.props.name;
    }

    render() {
        return (
            <div className="film-wrapper">
                <input type="text"
                       value={this.props.name}
                       className="film"
                       onChange={(e) => this.props.updateFilmHandler(e, this.props.id)}
                       />
                <button onClick={() => this.props.removeFilmHandler(this.props.id)} className="remove-btn" >X</button>
            </div>
        );
    }
}

export default Film;