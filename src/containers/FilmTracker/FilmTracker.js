import React, {Component} from 'react';
import Film from "../../components/Film/Film";
import './FilmTracker.css';
import FilmForm from "./FilmForm/FilmForm";
import { nanoid } from 'nanoid'

class FilmTracker extends Component {

    state = {
        films: [
            {id: nanoid(), name: 'Revolver'},
            {id: nanoid(), name: 'Matrix'},
            {id: nanoid(), name: 'Rrand blue'},
        ]
    }

    constructor(props) {
        super(props);
        this.updateFilmHandler = this.updateFilmHandler.bind(this);
        this.createFilmHandler = this.createFilmHandler.bind(this);
        this.removeFilmHandler = this.removeFilmHandler.bind(this);
    }

    createFilmHandler(film) {
        this.setState({
            films: [...this.state.films, {id: nanoid(), name: film}]
        });
    }

    updateFilmHandler(event, filmId) {
        const index = this.state.films.findIndex(film => film.id === filmId);
        const filmCopy = [...this.state.films];
        filmCopy[index].name = event.target.value;
        this.setState(filmCopy);
    }

    removeFilmHandler(id) {
        const filmsCopy = [...this.state.films];
        const index = filmsCopy.findIndex(film => film.id === id);
        filmsCopy.splice(index, 1);
        this.setState({films: filmsCopy});
        console.log(filmsCopy);
    }


    render() {
        return (
            <>
                <section className="films">
                    <FilmForm createFilmHandler={this.createFilmHandler} />
                    <p>To watch list:</p>
                    {this.state.films.map(film => {

                        return <Film key={film.id}
                                     id={film.id}
                                     name={film.name}
                                     updateFilmHandler={this.updateFilmHandler}
                                     removeFilmHandler={this.removeFilmHandler}/>
                    })}
                </section>
            </>
        );
    }
}

export default FilmTracker;