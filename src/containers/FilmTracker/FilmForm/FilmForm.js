import React, {Component} from 'react';
import './FilmForm.css';

class FilmForm extends Component {

    state = {
        name: ''
    };

    constructor(props) {
        super(props);
        this.inputChangeHandler = this.inputChangeHandler.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    inputChangeHandler(e) {
        this.setState({name: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.createFilmHandler(this.state.name);
        this.setState({name: ''});
    }

    render() {
        return (
            <form className="film-form" onSubmit={this.handleSubmit}>
                <div className="film-wrapper">
                    <input type="text" value={this.state.name} onChange={this.inputChangeHandler} required/>
                    <button className="add-btn">Add</button>
                </div>

            </form>
        );
    }
}

export default FilmForm;